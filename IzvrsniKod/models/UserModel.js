const db = require('../db')

//razred User enkapsulira korisnika aplikacije
module.exports = class User {
    
    //konstruktor korisnika
    constructor(username, password, firstname, lastname, phonenumber, email, availability, role, approved,userimage, stationID) {
        this.username = username;
        this.password = password;
        this.firstname = firstname;
        this.lastname = lastname;
        this.phonenumber = phonenumber;
        this.email = email;
        this.availability = availability;
        this.role = role;
        this.approved = approved;
        this.exists = undefined;
        this.userimage = userimage;
        this.stationID = stationID
    }

    //dohvat korisnika na osnovu korisničkog imena 
    static async fetchByUsername(username) {
        let results = await dbGetUserByName(username)
        let newUser = new User()

        if( results.length > 0 ) {
            newUser = new User(results[0].username, results[0].password, 
                results[0].firstname, results[0].lastname, results[0].phonenumber, results[0].email,
                results[0].availability, results[0].role, results[0].approved, results[0].userimage, results[0].idstation)
            newUser.exists = true;
        }
        return newUser
    }

    static async getNotApprovedUsers() {
        let results = await dbGetNotApprovedUsers()
        if (results.length > 0) {
            return results;
        }
    }

    static async changeAvail(username) {
        let results = await dbChangeAvail(username)
        if (results.length > 0) {
            return results;
        }
    }

    static async approveUser(username) {
        let results = await dbApproveUser(username)
        if (results.length > 0) {
            return results;
        }
    }

    static async setLeader(username) {
        let results = await dbSetLeader(username)
        if (results.length > 0) {
            return results;
        }
    }

    static async removeLeader(username) {
        let results = await dbRemoveLeader(username)
        if (results.length > 0) {
            return results;
        }
    }

    static async deleteUser(username) {
        let result = await dbDeleteUser(username)
        if(result === true) {
            return true;
        }
    }

    static async getStationID(stationName) {
        let result = await dbGetStationID(stationName)
        if(result !== undefined) {
            return result
        }
    }

    static async getStationName(stationID) {
        let result = await dbGetStationName(stationID)
        if(result !== undefined) {
            return result
        }
    }

    static async getAllstations() {
        let result = await dbGetAllStations()
        if(result !== undefined) {
            return result
        }
    }

    static async setStationLeader(idStation,username) {
        let results = await dbSetStationLeader(idStation,username)
        if (results.length > 0) {
            return results;
        }
    }

    static async getStationMembers (idStation){
        let results = await dbGetStationMembers(idStation)
        if (results.length > 0 ){
            return results;
        }
    }

    static async getAction (username){
        let results = await dbGetAction(username)
        if(results.length > 0) {
            results = results[0].idaction;
            return results
        }
    }

    static async getMission (username){
        let results = await dbGetMission(username)
        results = results[0].idmission
        return results
    }

    static async insertSkill(username, nameskill) {
        let results = await dbInsertSkill(username, nameskill)
        if (results.length > 0) {
            return results;
        }
    }

    static async insertLocation(idmission, lat,lang) {
        let results = await dbInsertLocation(idmission, lat,lang)
        if (results.length > 0) {
            return results;
        }
    }

    static async getUserSkills(username) {
        let results = await dbGetSkill(username)
        if (results.length > 0) {
            return results;
        }
    }

    static async getAvailableMissions(username) {
        let results = await dbGetAvailableMissions(username)
        if (results.length > 0) {
            return results;
        }
    }

    static async getAvailability(username) {
        let results = await dbGetAvailability(username)
        if (results.length > 0) {
            return results;
        }
    }

    static async takeMission(username, idmission) {
        let results = await dbTakeMission(username, idmission)
        if (results.length > 0) {
            return results;
        }
    }

    //da li je korisnik pohranjen u bazu podataka?
    isPersisted() {
        return this.exists !== undefined
    }

    //provjera zaporke
    checkPassword(password) {
        return this.password ? this.password === password : false
    }

    //pohrana korisnika u bazu podataka
    async persist() {
        try {
            let userUsername = await dbNewUser(this)
            this.username = userUsername
        } catch(err) {
            console.log("ERROR persisting user data: " + JSON.stringify(this))
            throw err
        }
    }
    
    // Izbjegavati koristenje jer prikazuje password , koristiti zbog kontrole 
    /* toString(){
        return `${this.username} ${this.password}`
    } */

}

//dohvat korisnika iz baze podataka na osnovu korisničkog imena (stupac username)
dbGetUserByName = async (username) => {
    const sql = `SELECT username, password, firstname, lastname, phonenumber, email, availability, role, approved, userimage, idstation
    FROM users WHERE username = '` + username + `'`;
    try {
        const result = await db.query(sql, []);
        return result.rows;
    } catch (err) {
        console.log(err);
        throw err
    }
};

//umetanje zapisa o korisniku u bazu podataka
dbNewUser = async (user) => {
    const sql = "INSERT INTO users (username, password, firstname, lastname, phonenumber, email, availability, role, approved,userimage, idstation) VALUES ('" +
        user.username + "', '" + user.password + "', '" + user.firstname + "', '" + user.lastname + "', '" +
        user.phonenumber + "', '" + user.email + "', '" + user.availability +  "', '" + user.role +  "', '" + user.approved + "','" + user.userimage + "'," + user.stationID +  ") RETURNING username";
    try {
        const result = await db.query(sql, []);
        return result.rows[0].username;
    } catch (err) {
        console.log(err);
        throw err
    }
}

//dohvacanje svih korisnika koji nisu odobreni
dbGetNotApprovedUsers = async () => {
    const sql = "SELECT role, firstname, lastname, username, password, email, phonenumber, namestation, userimage FROM users INNER JOIN station ON users.idstation = station.idstation WHERE approved = false";
    try {
        const result = await db.query(sql, []);
        return result.rows;
    } catch (err) {
        console.log(err);
        throw err
    }
}

//odobravanje korisnika
dbApproveUser = async (username) => {
    const sql = "UPDATE users SET approved = true WHERE username = '" + username + "' RETURNING approved";
    try {
        const result = await db.query(sql, []);
        return result.rows;
    } catch (err) {
        console.log(err);
        throw err
    }
}

dbSetLeader = async (username) => {
    const sql = "UPDATE users SET role = 'voditelj' WHERE username = '" + username + "' RETURNING approved";
    try {
        const result = await db.query(sql, []);
        return result.rows;
    } catch (err) {
        console.log(err);
        throw err
    }
}

dbRemoveLeader = async (username) => {
    const sql = "UPDATE users SET role = 'spasilac' WHERE username = '" + username + "' RETURNING approved";
    try {
        const result = await db.query(sql, []);
        return result.rows;
    } catch (err) {
        console.log(err);
        throw err
    }
}

//brisanje korisnika
dbDeleteUser = async (username) => {
    const sql = "DELETE FROM users WHERE username = '" + username + "'";
    try {
        const result = await db.query(sql, []);
        return true;
    } catch (err) {
        console.log(err);
        throw err
    }
}

//dohvacanje idStanice
dbGetStationID = async (namestation) => {
    const sql = "SELECT * FROM station WHERE namestation = '" + namestation + "'";

    try {
        const result = await db.query(sql, []);
        return result.rows
    } catch (err) {
        console.log(err);
        throw err
    }
}

//dohvacanje namestation
dbGetStationName = async (idstation) => {
    const sql = "SELECT namestation FROM station WHERE idstation = '" + idstation + "'";

    try {
        const result = await db.query(sql, []);
        return result.rows;
    } catch (err) {
        console.log(err)
        throw err
    }
}

dbGetAllStations = async () => {
    const sql = "SELECT * FROM station";

    try {
        const result = await db.query(sql, []);
        return result.rows;
    } catch (err) {
        console.log(err)
        throw err
    }
}

dbSetStationLeader = async (idStation,username) => {
    const sql = "UPDATE station SET nameleader ='"+ username+"' WHERE idStation = '" + idStation + "' RETURNING nameleader";
    try {
        const result = await db.query(sql, []);
        return result.rows;
    } catch (err) {
        console.log(err);
        throw err
    }
}

dbGetStationMembers = async (idStation) => {
    const sql = `SELECT username, password, firstname, lastname, phonenumber, email, availability, role, approved, userimage, idstation
    FROM users WHERE idstation = '` + idStation + `' AND role='spasilac' `;
    try {
        const result = await db.query(sql, []);
        return result.rows;
    } catch (err) {
        console.log(err);
        throw err
    }
}

dbInsertSkill = async (username, nameskill) => {    
    const sql =  `INSERT INTO hasSkills (username, idSkill) VALUES ('` + username + `',(SELECT idSkill FROM skills WHERE nameskill = '` + nameskill + `') `  + `)`;
    try {
        const result = await db.query(sql, []);
        return result.rows
    } catch (err) {
        console.log(err);
        throw err
    }
}

dbInsertLocation = async (idmission,lat,lang) => {    
    const sql =  `INSERT INTO location VALUES(now(), ` + lat + `,` + lang + `,` + idmission + `)`;
    try {
        const result = await db.query(sql, []);
        return result.rows
    } catch (err) {
        console.log(err);
        throw err
    }
}

dbGetSkill = async (username) => {
    const sql = `SELECT nameskill FROM hasskills JOIN skills ON hasskills.idSkill = skills.idSkill WHERE username = '` + username + `'`;
    try {
        const result = await db.query(sql, []);
        return result.rows
    } catch (err) {
        console.log(err);
        throw err
    }
}

dbChangeAvail = async (username) => {
    const sql = "UPDATE users SET availability = NOT availability WHERE username = '" + username + "' RETURNING approved";
    try {
        const result = await db.query(sql, []);
        return result.rows
    } catch (err) {
        console.log(err);
        throw err
    }
}

dbGetAvailableMissions = async (username) => {
    const sql = `SELECT DISTINCT mission.idaction, mission.assignment, mission.description, skills.nameskill, mission.idmission  
    FROM mission INNER JOIN hasskills ON mission.idskill = hasskills.idskill INNER JOIN skills ON hasskills.idskill = skills.idskill
    INNER JOIN action ON action.idaction = mission.idaction
    WHERE action.ongoingaction = true AND mission.username IS NULL 
    AND mission.idskill IN (SELECT idskill FROM hasskills WHERE username = '` + username + `')`;
    try {
        const result = await db.query(sql, []);
        return result.rows
    } catch (err) {
        console.log(err);
        throw err
    }
}

dbGetAvailability = async (username) => {
    const sql = "SELECT availability FROM users WHERE username = '" + username + "'";
    try {
        const result = await db.query(sql, []);
        return result.rows
    } catch (err) {
        console.log(err);
        throw err
    }
}

dbTakeMission = async (username, idmission) => {
    const sql = "UPDATE mission SET username = '" + username + "' WHERE idmission = " + idmission + " RETURNING username";
    try {
        const result = await db.query(sql, []);
        return result.rows
    } catch (err) {
        console.log(err);
        throw err
    }
}

dbGetAction = async (username) => {
    const sql = `SELECT DISTINCT(action.idaction) FROM users inner join mission on mission.username=users.username
    inner join action on action.idaction = mission.idaction WHERE users.username = '` + username + `' and
    action.ongoingaction=true`;
    try {
        const result = await db.query(sql, []);
        return result.rows
    } catch (err) {
        console.log(err);
        throw err
    }
}

dbGetMission = async (username) => {
    const sql = `SELECT mission.idmission FROM users inner join mission on mission.username=users.username
    inner join action on action.idaction = mission.idaction WHERE users.username = '` + username + `' and
    action.ongoingaction=true`;
    try {
        const result = await db.query(sql, []);
        return result.rows
    } catch (err) {
        console.log(err);
        throw err
    }
}


