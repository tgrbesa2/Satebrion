const { json } = require('express');
var express = require('express');
var router = express.Router();
const mission = require('../models/MissionModel');
const actionModel = require('../models/ActionModel');
const stationModel = require('../models/StationModel')
const User = require('../models/UserModel')

// Za glavnu stranicu (doček korisnika)
router.get('/', async function(req,res,next) {
    if(req.session.user.role === "dispecer") { 
        let ongoing = await actionModel.getOngoingActions(1)
        let locations = await mission.getLocations(ongoing[0].idaction)

        let locations2 = await stationModel.getAllStationLocation()
        for(let i = 0; i < ongoing.length; i++) {
            ongoing[i] = +ongoing[i].idaction
        }
        locations = locations.map((key) => [key.latitdue,Number(key.longitude), key.intensity, key.radius])
        locations2 = locations2.map((key) => [key.latitude,Number(key.longitude)])
        let imena = await stationModel.getAllstations()
        imena = imena.map((key) =>[key.namestation])

        tempStationsLocations = await stationModel.getTempStations()
        if(tempStationsLocations.length > 0) {
            tempStationsLocations = tempStationsLocations.map((key)=>[+key.latitdue, +key.longitude])
        }

        res.render('mapaDispecer', {
            title : 'Mapa', 
            user: req.session.user,
            error:undefined,
            locations: locations,
            locations2: locations2,
            start: locations2[0],
            svi: ongoing,
            imena:imena,
            tempStations: tempStationsLocations
        }); 
    } else if(req.session.user.role === "spasilac" || req.session.user.role === "voditelj"){
        let isOnAction = await User.getAction(req.session.user.username)
       
        if(isOnAction === undefined) res.redirect("/")
        else {
            let akcija = await User.getAction(req.session.user.username)
            akcija = +akcija
            let locations = await mission.getLocations(+akcija)

            let locations2 = await stationModel.getAllStationLocation()
            locations = locations.map((key) => [key.latitdue,Number(key.longitude), key.intensity, key.radius])
            locations2 = locations2.map((key) => [key.latitude,Number(key.longitude)])

            tempStationsLocations = await stationModel.getTempStations()
            if(tempStationsLocations.length > 0) {
                tempStationsLocations = tempStationsLocations.map((key)=>[+key.latitdue, +key.longitude])
            }

            let imena = await stationModel.getAllstations()
            imena = imena.map((key) =>[key.namestation])

            let currentMission = await mission.getOngoingUserMissions(req.session.user.username)
            //if(currentMission !== undefined) {
                currentMission = currentMission.map((key) => [key.assignment, Number(key.idmission)])
           // }
            
            let actionComment = Object();
            actionComment.username = "ALka"
            actionComment.comment = "Jede"
            actionComment.commentTime = "Puno vrenena"

            let assignments = await mission.getAllAssignments(req.session.user.username)
            assignments = assignments.map((key) => [Number(key.idassignment), key.assignmenttext])
            let assignmentLocations = new Map()
            for (let [key, value] of assignments) {
                assignmentLocation = await mission.getAssignmentLocation(key)
                assignmentLocations.set(key, assignmentLocation)
            }
        
            res.render('mapaUser', {
                title : 'Mapa', 
                user: req.session.user,
                error:undefined,
                locations: locations,
                locations2: locations2,
                start: locations2[0],
                actionComment: actionComment,
                imena:imena,
                akcija:akcija,
                misije: currentMission,
                zadaci: assignments,
                tempStations: tempStationsLocations,
                assignmentLocations: assignmentLocations
            });
        } 
    } else {
        res.redirect('/');
    }
});

router.post('/update', async function(req,res,next) {
    if(req.session.user.role === "dispecer") {
    let odabran = +Object.keys(req.body)[0]
    let locations = await mission.getLocations(odabran)
    let tempStations = await stationModel.getTempStations(odabran)

    tempStations = tempStations.map((key)=>[+key.latitdue, +key.longitude])
    locations = locations.map((key) => [key.latitdue,Number(key.longitude), key.intensity, key.radius])
    
    res.send({data:locations, tempstations:tempStations})
    }
});

router.post('/updateCurrent', async function(req,res,next) {
    if(req.session.user.role === "dispecer" || req.session.user.role==="spasilac" || req.session.user.role==="voditelj") {
    let odabran = +Object.keys(req.body)[0]
    let locations = await mission.getCurrentLocations(odabran)
    locations = locations.map((key) => [key.latitdue,Number(key.longitude), key.intensity, key.radius,key.username])
    res.send({data:locations})
    }
});

router.post('/updateMission', async function(req,res,next) {
    if(req.session.user.role === "spasilac" || req.session.user.role === "voditelj") {
        let ime = req.session.user.username
        console.log(req.body)
        for (var key of Object.keys(req.body)) {
            mission.finishAssignment(+key)
            if(req.body[key] == "Postavi privremenu postaju") {
                locations = await mission.getAssignmentLocation(+key)
                actionID = await mission.getActionWithAssignment(+key)
                longitude = locations[0].longitude
                latitude = locations[0].latitude
                actionID = actionID[0].idaction
                await stationModel.insertTempStation(+actionID, +longitude,+latitude)
            }
        }
        res.redirect('/mapaUser')
    }
});

router.post('/savePosition', async function(req,res,next) {
    if(req.session.user.role === "spasilac" || req.session.user.role==="voditelj") {
        let lok = Object.keys(req.body)[0]
        lok = lok.split(";")
        lok = lok.map((key) => +key)
        let misija = await User.getMission(req.session.user.username)
        if(misija) {
            let insert = await User.insertLocation(+misija, lok[0], lok[1])
        }
    }
});

router.post('/sendComment', async function(req, res, next) {
    (async () => {
        if(req.session.user.role === "spasilac" || req.session.user.role === "voditelj") {
        let tmp = await actionModel.getActionByUsername(req.session.user.username)
        let result = await actionModel.insertComment(req.session.user.username, req.body.comment, tmp[0].idaction)

        
        res.redirect('/mapaUser')
        }
    })();
});
router.post('/sendCommentDispecer', async function(req, res, next) {
    (async () => {
        if(req.session.user.role==="dispecer") {
        let comment = Object.keys(req.body) [0]
        comment = comment.split(",")
        //comment[0] je komentar, comment[1] je id action
        let result = await actionModel.insertComment(req.session.user.username, comment[0], comment[1])
        }
    })();
});

router.post('/refreshCommentsDispecer', async function (req, res, next) {
    (async () => {
        if(req.session.user.role === "dispecer") {
        let comments = await actionModel.getActionComments(Object.keys(req.body))
        

        res.send({
            data : comments
        })
    }
    })()
})

router.post('/refreshComments', async function (req, res, next) {
    (async () => {
        if(req.session.user.role === "spasilac" || req.session.user.role === "voditelj") {
        let tmp = await actionModel.getActionByUsername(req.session.user.username)
        let comments = await actionModel.getActionComments(tmp[0].idaction)

        res.send({
            data : comments
        })
    }
    })()
})



module.exports = router;