// npm paketi
var express = require('express');
var router = express.Router();
const User = require('../models/UserModel');
const Action = require('../models/ActionModel');
const Station = require('../models/StationModel');
const mission = require('../models/MissionModel');
const Mission = require('../models/MissionModel');
var tmpLatitude = undefined;
var tmpLongitude = undefined;

router.get('/', function(req,res,next) {
    (async () => {
        if (req.session.user.role !== "dispecer"){
            res.redirect("/")
        } else {
            res.render('home', {
                user:req.session.user
            });
        }
    })();
})

router.get('/otvoriakciju', function(req,res,next) {
    (async () => {
        
        if(req.session.user.role==="dispecer") {
        res.render('otvoriAkciju', {
            user:req.session.user
        })
    } else {
        res.redirect("/")
    }
    })();
})


router.post('/otvoriakciju', function(req, res, next) {
    (async () => {
        if(req.session.user.role==="dispecer") {
        Action.insertNewAction(req.body.naslovakcije, req.body.opis)

        res.redirect('/dispecer/akcije')
        } else {
            res.redirect("/")
        }
    })();
})


router.get('/akcije' , function(req,res,next) {
    (async () => {
        if(req.session.user.role === "dispecer") {
        let akcije = await Action.getOngoingActionsData();
        var spasioci = [];
        for(var i = 0; i < akcije.length; i++)
        {
            spasioci.push(await Action.getAllRescuers(akcije[i].idaction))
        }
        res.render('akcije' , {
            title: "Akcije",
            user:req.session.user,
            actions: akcije,
            spasioci: spasioci
        })
    } else {
        res.redirect("/")
    }
    })();
})

router.get('/akcije/:idakcije/zatrazi', function(req,res,next) {
    (async () => {
        //console.log(req.params.idakcije) Kontrola
        let akcija = await Action.fetchById(req.params.idakcije);
        akcija.idaction = req.params.idakcije; //bez ovoga idaction bude undefined
        //console.log(akcija);
        res.render('zatraziUkljucivanje' , {
            user:req.session.user,
            title:'Zahtjev za ukljucivanjem',
            akcija:akcija
        })
    })();
})

//Prikaz stranice za naslov i opis zadatka za pojedinog spasioca
router.get('/akcije/:idakcije/dodajZadatak/:spasioc', function(req,res,next) {
    (async () => {
        if(req.session.user.role === "dispecer") {
        let akcija = await Action.fetchById(req.params.idakcije);
        akcija.idaction = req.params.idakcije;
        res.render('dodajZadatak' , {
            user:req.session.user,
            title:'Dodaj Zadatak',
            akcija:akcija,
            spasioc: req.params.spasioc
        })
    } else {
        res.redirect("/")
    }
    })();
}) 

router.get ('/spasioci' , function (req,res,next) {
    (async () => {
        if(req.session.user.role === "dispecer") {
        let postaje = await Station.getAllstations()
        var spasioci = []
        
        for (let i=0; i<postaje.length ; i++){
            spasioci.push(await User.getStationMembers(postaje[i].idstation)) // Daje undefined ako nema spasioca neke postaje
        } 
        //console.log(postaje)
        //console.log(spasioci)
        res.render('dostupniSpasioci', {
            user:req.session.user,
            title:'Dostupni spasioci',
            postaje:postaje,
            spasioci:spasioci
        })
    } else {
        res.redirect("/")
    }
    })();
})

//TODO: POST metoda za objavljivanje zahtjeva za ukljucivanje
router.post('/akcije/:idakcije/zatrazi', function(req,res,next) {
    (async () => {
        if(req.session.user.role === "dispecer") {
        // Treba napisat funkciju za spremanje zadatke -> pretpostavljam u novu tablicu baze
       //let zahtjev = new mission(undefined, req.params.idakcije, req.body.osposobljenja);
        //let rez = await zahtjev.persist();
        mission.newMission(req.params.idakcije, req.body.osposobljenje)
        res.redirect('/dispecer/akcije')
        }
    })();
})


//Za uklanjanje spasioca s akcije 
router.post('/akcije/:idakcije/ukloni/:spasioc', function(req,res,next) {
    (async () => {
        if(req.session.user.role === "dispecer") {
        let idRemoval = await Mission.getGetMissionIDByUserAndAction(req.params.idakcije, req.params.spasioc)
        await Action.removeRescuer(idRemoval)
        await User.changeAvail(req.params.spasioc)
        res.redirect('/dispecer/akcije')
        }
    })();
})

router.post('/akcije/:idakcije/dodajZadatak/:spasioc', function(req,res,next){
    (async() => {
        if(req.session.user.role === "dispecer") {
        let misijaID = await dbGetMissionIDByUserAndAction(req.params.spasioc, req.params.idakcije) 
        let add = await dbAddAssignment(misijaID, req.body.zadatak, req.body.opiszadatka, tmpLongitude, tmpLatitude)
        res.redirect('/dispecer/akcije')
        }
    })();
})


router.post ('/akcije/:idakcije/zavrsi',function(req,res,next) {
    (async () => {
        if(req.session.user.role === "dispecer") {
        let finished = await Action.finishAction(req.params.idakcije)
        res.redirect('/dispecer/akcije')
        }
    })();
})

router.post('/akcije/:idakcije/assignmentLocation', function(req, res, next) {
    (async () => {
        if(req.session.user.role === "dispecer") {
        let tmp = Object.keys(req.body)[0].split("(")[1].split(", ")
        tmpLongitude = tmp[0];
        tmpLatitude = tmp[1].substring(0, tmp[1].length - 1)
        }
        
    })();
})

router.post('/akcije/:idakcije/dodajZadatak/zad/assignmentLocation', function(req, res, next) {
    (async () => {
        if(req.session.user.role === "dispecer") {
        let tmp = Object.keys(req.body)[0].split("(")[1].split(", ")
        tmpLongitude = tmp[0];
        tmpLatitude = tmp[1].substring(0, tmp[1].length - 1)
        }
        
    })();
})

router.get('/logout', function (req, res, next) {
    (async () => {
        res.redirect('/logout');

    })();
})

router.get('/userprofile', function (req, res, next) {
    (async () => {
        res.redirect('/userprofile');

    })();
})


module.exports = router;
