var express = require('express');
var router = express.Router();
const User = require ('../models/UserModel');

router.get('/', function(req,res,next) {
    if(req.session.user === undefined) {
        res.render('register',{
            title:"Registracija",
            user: req.session.user,
            error:undefined
        });
    } else {
        res.redirect('/');
    }
});

router.post('/' , (req,res,next) => {
    (async () => {
        if(req.session.user === undefined) {
        let user = await User.fetchByUsername(req.body.username)

        if(user.username !== undefined) {
            res.render('register', {
                title: "Registracija",
                user:req.session.user,
                error:"Korisničko ime je zauzeto"
            })
            return
        }

        let stationID = await User.getStationID(req.body.stanica)

        
        try {
            user = new User(req.body.username,
                req.body.password,
                 req.body.ime,
                 req.body.prezime,
                 req.body.mobitel,
                 req.body.email,
                 true,
                 req.body.radiobox,
                 false,
                 req.files.picture.data.toString('base64'),
                 stationID[0].idstation);
             await user.persist()
        } catch(e) {
            if (e.constraint == "atributes2_unique") {
                res.render('register', {
                    title: "Registracija",
                    user:req.session.user,
                    error:"Unesena e-mail adresa se već koristi"
                })
                return
            } else if(e.constraint == "atributes1_unique") {
                res.render('register', {
                    title: "Registracija",
                    user:req.session.user,
                    error:"Uneseni broj mobitela se već koristi"
                })
                return
            } else {
                res.render('register', {
                    title: "Registracija",
                    user:req.session.user,
                    error:"Vaš unos nije ispravan."
                })
                return
            }
        }
        res.redirect('/')
    }
    })();
})
module.exports=router;