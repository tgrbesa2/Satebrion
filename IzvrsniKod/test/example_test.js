var request = require('supertest')
var chai = require('chai')
var chaiHttp = require('chai-http')
var expect = chai.expect
const db = require('../db/index')

chai.use(chaiHttp)
chai.should()

describe('express app testing', function() {
    this.timeout(10000)
    var server;
    beforeEach(function () {

        server = require('../server')
    })

    afterEach(function (done) {
        delete require.cache[require.resolve('../server')];
        server.close()

        done()
    })

    
    after(function (done) {
        //db.query("DELETE FROM action WHERE actiontitle = 'spasavanje macke test'")
        done()
    })


    it('Test response to /', function(done) {
        chai.request(server)
        .get('/')
        .end((err,res) => {
            expect(res.status).equal(200)
            done()
        })
    })

    it('Test response to foo/bar', function(done) {
        chai.request(server)
        .get('/foo/bar')
        .end((err,res) => {
            expect(res.status).equal(404)
            done()
        })
    })

})