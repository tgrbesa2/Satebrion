const db = require('../db')

//razred User enkapsulira korisnika aplikacije
module.exports = class Station {
    
    //konstruktor korisnika
    constructor(idStation, nameStation, nameLeader, latitude, longitude) {
        this.idStation = idStation;
        this.nameStation = nameStation;
        this.nameLeader = nameLeader;
        this.latitude = latitude;
        this.longitude = longitude;
        this.exists = undefined;
    }

    //dohvat korisnika na osnovu korisničkog imena 
    static async fetchByID(id) {
        let results = await dbGetStationByID(id)
        let station = new Station()

        if( results.length > 0 ) {
            station = new Station(results[0].idstation, results[0].namestation, 
                results[0].nameleader, results[0].latitude, results[0].longitude)
            station.exists = true;
        }
        return station
    }

    static async getStationID(stationName) {
        let result = await dbGetStationID(stationName)
        if(result !== undefined) {
            return result
        }
    }

    static async getStationName(stationID) {
        let result = await dbGetStationName(stationID)
        if(result !== undefined) {
            return result
        }
    }

    static async getAllstations() {
        let result = await dbGetAllStations()
        if(result !== undefined) {
            return result
        }
    }

    static async getTempStations() {
        let result = await dbGetTempStations()
        if(result !== undefined) {
            return result
        }
    }

    static async insertTempStation(idaction, long,lat) {
        let result = await dbInsertTempStation(idaction,long,lat)
        if(result !== undefined) {
            return result
        }
    }
    
    static async GetStationByLeader(nameLeader) {
        let result = await dbGetStationByLeader(nameLeader)
        if(result !== undefined) {
            return result
        }
    }

    static async setStationLeader(idStation,username) {
        let results = await dbSetStationLeader(idStation,username)
        if (results.length > 0) {
            return results;
        }
    }
    static async getAllStationLocation() {
        let results = await dbGetAllStationsLocation()
        if(results.length > 0) {
            return results
        }
    }
    //metoda za dobivanje popisa spasilaca po stanici
    static async getUsersByStations(namestation) {
        let results = await dbGetUsersByStationName(namestation);
        if(results.length > 0){
            return results
        }
    }

    //da li je korisnik pohranjen u bazu podataka?
    isPersisted() {
        return this.exists !== undefined
    }

    //provjera zaporke
    checkPassword(password) {
        return this.password ? this.password === password : false
    }

    //pohrana korisnika u bazu podataka
    async persist() {
        try {
            let userUsername = await dbNewUser(this)
            this.username = userUsername
        } catch(err) {
            console.log("ERROR persisting user data: " + JSON.stringify(this))
            throw err
        }
    }


}

dbGetStationByID = async (id) => {
    const sql = `SELECT idStation, namestation,nameleader,latitude,longitude FROM station WHERE idstation = '` + id + `'`;
    try {
        const result = await db.query(sql, []);
        return result.rows;
    } catch (err) {
        console.log(err);
        throw err
    }
};

//dohvacanje idStanice
dbGetStationID = async (namestation) => {
    const sql = "SELECT * FROM station WHERE namestation = '" + namestation + "'";

    try {
        const result = await db.query(sql, []);
        return result.rows
    } catch (err) {
        console.log(err);
        throw err
    }
}

//dohvacanje namestation
dbGetStationName = async (idstation) => {
    const sql = "SELECT namestation FROM station WHERE idstation = '" + idstation + "'";

    try {
        const result = await db.query(sql, []);
        return result.rows;
    } catch (err) {
        console.log(err)
        throw err
    }
}

dbGetAllStations = async () => {
    const sql = "SELECT idstation,namestation,nameleader FROM station";

    try {
        const result = await db.query(sql, []);
        return result.rows;
    } catch (err) {
        console.log(err)
        throw err
    }
}

dbGetAllStationsLocation = async () => {
    const sql = "SELECT latitude,longitude FROM station";

    try {
        const result = await db.query(sql, []);
        return result.rows;
    } catch (err) {
        console.log(err)
        throw err
    }
}

dbGetStationByLeader = async (nameLeader) => {
    const sql = "SELECT namestation FROM station WHERE nameLeader = '" + nameLeader + "'";
    try {
        const result = await db.query(sql, []);
        return result.rows;
    } catch (err) {
        console.log(err)
        throw err
    }
}

dbSetStationLeader = async (idStation,username) => {
    const sql = "UPDATE station SET nameleader ='"+ username+"' WHERE idStation = '" + idStation + "' RETURNING nameleader";
    try {
        const result = await db.query(sql, []);
        return result.rows;
    } catch (err) {
        console.log(err);
        throw err
    }
}

//dohvacanje spasilaca po imenu stanice
dbGetUsersByStationName = async (stationName) => {
    const sql = "SELECT username, firstname, lastname, namestation FROM users INNER JOIN station ON users.idstation=station.idstation WHERE users.role = 'spasilac' AND namestation = '"+ nameStation + "'";
    try {
        const result = await db.query(sql, []);
        return result.rows;
    } catch (err) {
        console.log(err)
        throw err
    }
}

dbInsertTempStation = async (idaction,longitude, latitude) => {
    const sql = "Insert into tempstation(idaction, latitdue,longitude) values(" + idaction+", " + latitude+ ", " + longitude + ");" 
    try {
        const result = await db.query(sql, []);
        return result.rows;
    } catch (err) {
        console.log(err)
        throw err
    }
}

dbGetTempStations = async (idaction) => {
    const sql = `select latitdue,longitude from tempstation join action on action.idaction = tempstation.idaction where action.ongoingaction=True`
    try {
        const result = await db.query(sql, []);
        return result.rows;
    } catch (err) {
        console.log(err)
        throw err
    }
}