const db = require('../db')

module.exports = class Action {
    
    //konstruktor akcije
    constructor(ongoingaction, description, actiontitle) {
        this.idaction = undefined;
        this.ongoingaction = ongoingaction;
        this.description = description;
        this.actiontitle = actiontitle;
    }

    //dohvat akcije na osnovu ID-a
    static async fetchById(idaction) {

        let results = await dbGetActionById(idaction)
        let newAction = new Action()

        if( results.length > 0 ) {
            newAction = new Action(results[0].ongoingaction, results[0].description, results[0].actiontitle)
            newAction.id = results[0].id; 
            newAction.ongoingaction = results[0].ongoingaction;
        }
        return newAction
    }

    static async checkUserAction(username) {
        let results = await dbCheckUserAction(username)
        if(results.length > 0) {
            return true
        } else {
            return false
        }
    }

    static async getOngoingActions(broj) {
        let results = await dbGetOngoingActions()
        return results
    }

    static async getOngoingActionsData() {
        let results = await dbGetOngoingActionsData()
        if (results.length > 0) {
            return results;
        }
    }
    
    static async insertNewAction(actiontitle, actiondescription) {
        let results = await dbNewAction(actiontitle, actiondescription)
        if(results.length > 0) {
            return true
        } else {
            return false
        }
    }

    static async getAllRescuers(idaction) {
        let results = await dbGetAllRescuers(idaction)
        if (results.length > 0) {
            return results;
        }
    }

    //metoda za dohvacanje svih misija po akciji i skillu spasioca koji salje zahtjev
    static async getAllMissions(idaction, idskill) {
        let results = await dbGetMissions(idaction, idskill);
        if (results.length > 0) {
            return results;
        }
    }

    static async removeRescuer(idmission) {
        let results = await dbRemoveRescuer(idmission)

    }

    static async finishAction(idaction){
        let results = await dbFinishAction(idaction)
        if(results.length > 0) {
            return true
        } else {
            return false
        }
    }
    
    static async getActionByUsername(username) {
        let results = await dbGetActionByUsername(username);
        if(results.length > 0) {
            return results
        } else {
            return false;
        }
    }

    static async insertComment(username, comment, idaction) {
        let results = await dbAddActionComment(username, comment, idaction);
        if(results.length > 0) {
            return results
        } else {
            return false
        }
    }

    static async getActionComments(idaction) {
        let results = await dbGetActionComments(idaction)
        if(results.length > 0) {
            return results;
        } else {
            return false;
        }
    }


    //da li je korisnik pohranjen u bazu podataka?
    isPersisted() {
        return this.idaction !== undefined
    }


    //pohrana korisnika u bazu podataka
    async persist() {
        try {
            let idaction = await dbNewAction(this)
            this.idaction = idaction
        } catch(err) {
            console.log("ERROR persisting action data: " + JSON.stringify(this))
            throw err
        }
    }

}

//dohvat akcije iz baze podataka na osnovu ID-a
dbGetActionById = async (idaction) => {
    const sql = `SELECT idaction, actiontitle, ongoingaction, description
    FROM action WHERE idaction = '` + idaction + `'`;
    try {
        const result = await db.query(sql, []);
        return result.rows;
    } catch (err) {
        console.log(err);
        throw err
    }
};

dbGetOngoingActions = async () => {
    const sql = "SELECT idaction FROM action where action.ongoingaction=true";
    try {
        const result = await db.query(sql, []);
        return result.rows;
    } catch (err) {
        console.log(err);
        throw err
    }
};

dbGetOngoingActionsData = async () => {
    const sql = "SELECT idaction, actiontitle, description FROM action where action.ongoingaction=true";
    try {
        const result = await db.query(sql, []);
        return result.rows;
    } catch (err) {
        console.log(err);
        throw err
    }
};



//umetanje zapisa o akciji u bazu podataka
dbNewAction = async (actiontitle, actiondescription) => {
    const sql = "INSERT INTO action (ongoingaction, actiontitle, description) VALUES (true, '"
      + actiontitle + "', '" + actiondescription + "') RETURNING idaction";
    try {
        const result = await db.query(sql, []);
        return result.rows[0].idaction;
    } catch (err) {
        console.log(err);
        throw err
    }
}

//zavrsavanje akcije
dbFinishAction = async (actionid) => {
    const sql = "UPDATE action SET ongoingaction = false WHERE idaction = " + actionid;
    try {
        const results = await db.query(sql, []);
        return results.rows;
    } catch (err) {
        console.log(err);
        throw err
    }
}

dbCheckUserAction = async (username) => {
    const sql = "select ongoingaction from mission inner join action on mission.idaction = action.idaction where username='" + username + "' and ongoingaction=true;";
    try {
        const results = await db.query(sql, []);
        return results.rows;
    } catch (err) {
        console.log(err);
        throw err
    }
}

//dohvat svih spasioca na akciji
dbGetAllRescuers = async (idaction) => {
    const sql = `SELECT mission.username, firstname, lastname
    FROM mission INNER JOIN users ON mission.username = users.username
    WHERE idaction = '` + idaction + `'`;
    try {
        const result = await db.query(sql, []);
        return result.rows;
    } catch (err) {
        console.log(err);
        throw err
    }
}
//dohvacanje popisa misija po akciji i skillu spasioca koji trazi popis
dbGetMissions = async (idaction, idskill) => {
    const sql = `SELECT assignment, mission.description, users.username
    FROM mission INNER JOIN action ON mission.idaction = action.idaction INNER JOIN hasskills ON mission.idskill = hasskills.idskill INNER JOIN users ON hasskills.username = users.username 
    WHERE action.idaction =` + idaction + ` AND availability = true AND hasskills.idskill = ` + idskill;
    try {
        const result = await db.query(sql, []);
        return result.rows;
    } catch (err) {
        console.log(err);
        throw err
    }
}

dbRemoveRescuer = async (idmission) => {
    const sql = "DELETE FROM location WHERE idmission = " + idmission+";" +
               "DELETE FROM assignment WHERE idmission = " + idmission+";"+
               "DELETE FROM mission WHERE idmission = " + idmission;
    try {
        const result = await db.query(sql, []);
        return result.rows;
    } catch (err) {
        console.log(err);
        throw err
    }
}

dbGetActionByUsername = async (username) => {
    const sql = `SELECT action.idaction FROM action INNER JOIN mission ON mission.idaction = action.idaction
                WHERE mission.username = '` + username + `' AND action.ongoingaction = TRUE`;
    try {
        const result = await db.query(sql, []);
        return result.rows;

    } catch (err) {
        console.log(err);
        throw err;
    }
}

dbAddActionComment = async (username, comment, idaction) => {
    const sql = `INSERT INTO actioncomments (username, comment, idaction, commenttimestamp) VALUES
    ('` + username + `', '` + comment +`', ` + idaction + `, LOCALTIMESTAMP) RETURNING commenttimestamp`;
    
    try {
        const result = await db.query(sql, []);
        return result.rows;

    } catch (err) {
        console.log(err);
        throw err;
    }
}

dbGetActionComments = async(idaction) => {
    const sql = `SELECT idaction , TO_CHAR (commenttimestamp , 'YYYY-MM-DD HH24:MI:SS') commenttimestampconverted,username , comment FROM actioncomments WHERE idaction = ` + idaction;
    
    try {
        const result = await db.query(sql, []);
        return result.rows;

    } catch (err) {
        console.log(err);
        throw err;
    }
}