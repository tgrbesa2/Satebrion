const db = require('../db')

module.exports = class Mission {
    
    //konstruktor misije
    constructor(username, idaction, missionvehicle, longitude, latitude) {
        this.username = username;
        this.idaction = idaction;
        this.missionvehicle = missionvehicle;
        this.exists = undefined;
    }

    //dohvat misije na osnovu usernamea i ID-a
    static async fetchByUsernameAndId(username, idaction) {

        let results = await dbGetMissionByUserAndAction(username, idaction)
        let newMission = new Mission()

        if( results.length > 0 ) {
            newMission = new Mission(results[0].username, results[0].idaction, results[0].missionvehicle)
            newMission.exists = true;
        }
        return newMission
    }

    static async newMission(idaction, missionvehicle) {
        let results = await dbNewMission(idaction, missionvehicle)
        return results
    }

    static async getLocations(idaction) {

        let results = await dbGetLocations(idaction)
        return results
    }

    static async getOngoingUserMissions(username) {
        let results = await dbGetOngoingUserMissions(username)
        return results
    }

    static async deleteMission(missionid) {
        let results = await dbDeleteMission(missionid);
    }
    static async getCurrentLocations(mission) {
        let results = await dbGetCurrentLocations(mission)
        return results
    }
    static async getAllAssignments(username){
        let results = await dbGetAllAssignment(username)
        return results
    }
    static async finishAssignment(idassignment){
        let result = await dbSetFinishedToAssignment(idassignment)
    }

    static async getAssignmentLocation(idass) {
        let result = await dbGetAssignmentLocation(idass)
        return result
    }

    static async getActionWithAssignment(idass) {
        let result = await dbGetActionWithAssignment(idass)
        return result
    }

    static async getGetMissionIDByUserAndAction(idaction, username) {
        let result = await dbGetMissionIDByUserAndAction(username, idaction)
        return result
    }


    //da li je misija pohranjena u bazu podataka?
    isPersisted() {
        return this.exists !== undefined
    }


    //pohrana misije u bazu podataka
    async persist() {
        try {
            let username, idaction = await dbNewMission(this)
            this.username = username
            this.idaction = idaction
        } catch(err) {
            console.log("ERROR persisting mission data: " + JSON.stringify(this))
            throw err
        }
    }

}

//dohvat misije iz baze podataka na osnovu usernamea i ID-a
dbGetMissionByUserAndAction = async (username, idaction) => {
    const sql = `SELECT username, idaction, assignment, description, missionvehicle
    FROM mission WHERE username = '` + username + `' AND idaction = ` + idaction;
    try {
        const result = await db.query(sql, []);
        return result.rows;
    } catch (err) {
        console.log(err);
        throw err
    }
};

dbGetOngoingUserMissions = async(username) => {
    const sql = `select mission.idmission,mission.assignment from mission inner join action on mission.idaction = action.idaction
    where action.ongoingaction = true and mission.username = '`+username +`'`;
    try {
        const result = await db.query(sql, []);
        return result.rows;
    } catch (err) {
        console.log(err);
        throw err
    }
}


//umetanje zapisa o misiji u bazu podataka
dbNewMission = async (actionid, missionvehicle) => {
    const sql = "INSERT INTO mission (idaction, idskill) VALUES ('"
         + actionid + "', '" + missionvehicle +  "') RETURNING username, idaction";
    try {
        const result = await db.query(sql, []);
        return result.rows[0].username, result.rows[0].idaction;
    } catch (err) {
        console.log(err);
        throw err
    }
}

dbDeleteMission = async (missionid) => {    
    const sql =  `UPDATE mission set assignment=NULL`;
    try {
        const result = await db.query(sql, []);
        return result.rows
    } catch (err) {
        console.log(err);
        throw err
    }
}

dbGetLocations = async (idaction) => {
    const sql = `Select latitdue, location.longitude, intensity, radius from location inner join mission on mission.idmission = location.idmission
    inner join skills on mission.idskill = skills.idskill
    where idaction = '` + idaction+ `' 
    and floor(extract(epoch from now()-pingtime)/3600) < 2;`
    try {
        const result = await db.query(sql, []);
        return result.rows;
    } catch (err) {
        console.log(err);
        throw err
    }
}

dbGetCurrentLocations = async (idaction) => {
    const sql = `Select DISTINCT ON (mission.username) mission.username, location.latitdue, location.longitude, intensity, 
    radius from location inner join mission on mission.idmission = location.idmission
        inner join skills on mission.idskill = skills.idskill
        inner join users on mission.username = users.username
        where idaction = '` + idaction+ `'
        and floor(extract(epoch from now()-pingtime)/3600) < 2
        order by mission.username, location.pingtime desc;`
    try {
        const result = await db.query(sql, []);
        return result.rows;
    } catch (err) {
        console.log(err);
        throw err
    }
}

dbAddAssignment = async (idmission, assignmenttext, description, longitude, latitude) => {
    const sql = "INSERT INTO assignment (idmission, assignmenttext, description, longitude, latitude, finished) VALUES ('"
     + idmission + "', '" + assignmenttext + "', '" + description+ "', '" + longitude + "', '" + latitude +"'," + "false )"
    try{
        const result = await db.query(sql, []);
    } catch (err) {
        console.log(err);
        throw err
    }
}

dbGetMissionIDByUserAndAction = async (username, idaction) => {
    const sql = `SELECT idmission FROM mission WHERE username = '` + username + `' AND idaction = ` + idaction;
    try {
        const result = await db.query(sql, []);
        return result.rows[0].idmission;
    } catch (err) {
        console.log(err);
        throw err
    }
};

//dohvacanje svih zadataka na misiji
dbGetAllAssignment = async(username)=>{
    const sql ="select mission.username, mission.idmission, idassignment, assignmenttext from mission inner join assignment on mission.idmission = assignment.idmission where finished = false and username = '" + username + "'";
    try {
        const result = await db.query(sql, []);
        return result.rows;
    } catch (err) {
        console.log(err);
        throw err
    }
};
//promjena zadatka u izvršen
dbSetFinishedToAssignment = async(idassignment) => {
    const sql =  `UPDATE assignment set finished=true WHERE idassignment = ` + idassignment;
    try {
        const result = await db.query(sql, []);
        return result.rows
    } catch (err) {
        console.log(err);
        throw err
    }
}

dbGetAssignmentLocation = async(idassignment) => {
    const sql =  `Select latitude,longitude from assignment where idassignment = ` + idassignment;
    try {
        const result = await db.query(sql, []);
        return result.rows
    } catch (err) {
        console.log(err);
        throw err
    }
}

dbGetActionWithAssignment = async(idassignment) => {
    const sql =  `select action.idaction from assignment join mission on mission.idmission = 
    assignment.idmission join action on action.idaction = mission.idaction where
    idassignment = ` + idassignment;
    try {
        const result = await db.query(sql, []);
        return result.rows
    } catch (err) {
        console.log(err);
        throw err
    }
}