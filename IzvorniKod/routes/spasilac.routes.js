var express = require('express')
var router = express.Router();
const pg = require('pg');
const User = require('../models/UserModel');
const db = require('../db');
const Action = require('../models//ActionModel')

// Dohvat glavne stranice spasilaca
router.get('/', function(req,res,next){
    (async () => {
        if (req.session.user.role !== "spasilac"){
            res.redirect("/")
        } else {
            console.log("spasilac route!")
            res.render('spasilac', {
                title:"HGSS Spasilac",
                user:req.session.user
            });
        }
    })();
})

router.get('/userprofile', function (req, res, next) {
    (async () => {
        res.redirect('/userprofile');

    })();
})

router.get('/logout', function (req, res, next) {
    (async () => {
        res.redirect('/logout');

    })();
})

router.get('/zahtjevi', function (req, res, next) {
    (async () => {
        if (req.session.user.role === "spasilac"){
        let misije = await User.getAvailableMissions(req.session.user.username)
        let dostupnost = await User.getAvailability(req.session.user.username)
        res.render('zahtjeviSpasilac' , {
            user:req.session.user,
            title:'Dostupni zahtjevi za spašavanje',
            missions: misije,
            availability: dostupnost
        })
        
    }     else {
        res.redirect("/")
    }
    })();
})

router.post('/zahtjevi/:idmission', function (req, res, next) {
    (async () => {
        if (req.session.user.role === "spasilac"){
        let spasioc = req.session.user.username
        let x = await User.takeMission(spasioc, req.params.idmission)
        let y = await User.changeAvail(spasioc)
        let user = await User.fetchByUsername(spasioc)
        req.session.user = user
        res.redirect('/')
        }else {
            res.redirect("/")
        }
    })();
})

router.post('/dostupnost', function(req,res,next) {
    (async () => {
        if (req.session.user.role === "spasilac"){
        let ime = req.body.ime
        let provjera = await Action.checkUserAction(ime)
        if(provjera) {
            res.redirect('/')
        } else {
            let promjena = await User.changeAvail(ime)
            let user = await User.fetchByUsername(ime)
            req.session.user = user
            res.redirect('/')
        }
    }else {
        res.redirect("/")
    }
    })();
})

router.get('/akcije' , function(req,res,next) {
    (async () => {
        if (req.session.user.role === "spasilac"){
        res.render ('spasilacAkcije', {
            title:'Zahtjevi za spasavanje',
            user:req.session.user
        })
    }else {
        res.redirect("/")
    }
    })();
})

module.exports = router;