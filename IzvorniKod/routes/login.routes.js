var express = require('express');
var router = express.Router();
const User = require ('../models/UserModel');

// Prikaz login stranice
router.get('/', function(req,res,next) {
    if(req.session.user === undefined) {
        res.render('login', {
            title : 'Login',
            user: req.session.user,
            error:undefined
        });
    } else {
        res.redirect('/');
    }
});


router.post('/', function (req, res, next) {
    (async () => {
        if(req.session.user === undefined) {
        let user = await User.fetchByUsername(req.body.username);
        if (user.approved === false) {
            res.render('login', {
                title: 'Login',
                linkActive: 'login',   
                user: req.session.user,
                error: 'Korisnik nije potvrđen ili ne postoji',
            });
            return;
        }

        if (!user.checkPassword(req.body.password)) {
            res.render('login', {
                title: 'Login',
                linkActive: 'login',
                user: req.session.user,
                error: 'Kriva šifra ili korisničko ime',
            });
            return;
        }

        req.session.user = user;
        res.redirect('/')
    }
    })();
});

module.exports = router;