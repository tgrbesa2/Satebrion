const express = require('express');
const router = express.Router();

router.get('/', function (req, res, next) {
   if(req.session.user !== undefined) {
   req.session.user = undefined // Brise trenutnog korisnika sessiona

   req.session.destroy((err) => {

      if (err) {
         console.log(err)   //javlja error ako do njega dode
      } else {
         res.redirect('./') // redirect na home page
      }
   })
}
});

module.exports = router;