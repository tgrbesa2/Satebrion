var express = require('express');
var router = express.Router();

// Za glavnu stranicu (doček korisnika)
router.get('/', function(req,res,next) {
    res.render('home', {
        title : 'Home',
        user: req.session.user
    })
});



module.exports = router;