// npm paketi
var express = require('express');
var router = express.Router();
const User = require('../models/UserModel');
const Station = require('../models/StationModel')

// Dohvat glavne stranice administratora
router.get('/', function (req, res, next) {
    (async () => {
        if (req.session.user.role !== "admin") {
            res.redirect("/")
        } else {
            console.log("admin route!")
            res.render('admin', {
                title: "Admin homepage",
                user: req.session.user
            });
        }
    })();
})

router.get('/logout', function (req, res, next) {
    (async () => {
        res.redirect('/logout');

    })();
})


router.get('/zahtjevi', function (req, res, next) {
    (async () => {
        if(req.session.user.role === "admin") {
        let notApproved = await User.getNotApprovedUsers();

        res.render('zahtjevi', {
            title: "Zahtjevi za registraciju",
            user: req.session.user,
            notApprovedUsers: notApproved
        });
    } else {
        res.redirect("/")
    }

    })();
})
router.get('/userprofile', function (req, res, next) {
    (async () => {
        res.redirect('/userprofile');

    })();
})

router.get('/stanice', function (req, res, next) {
    (async () => {
        if(req.session.user.role === "admin") {
        let stations = await Station.getAllstations();
        res.render('stanice', {
            title: "Postavljanje voditelja stanica",
            user: req.session.user,
            stations: stations,
            err:undefined
        });
    } else {
        res.redirect("/")
    }

    })();
})


router.post('/stanice', function (req, res, next) {
    (async () => {
        if(req.session.user.role === "admin") {
        let stations = await Station.getAllstations();
        if (req.body.dodajButton !== undefined) {
            let user = await User.fetchByUsername(req.body.leader)

            if(user.exists==undefined) {
                console.log("error1")
                res.render('stanice', {
                    title: "Postavljanje voditelja stanica",
                    user: req.session.user,
                    stations: stations,
                    err: "Osoba ne postoji"
                });
            }
            else if(user.approved==false || user.role!="spasilac") {
                console.log("error2")
                res.render('stanice', {
                    title: "Postavljanje voditelja stanica",
                    user: req.session.user,
                    stations: stations,
                    err: "Osoba nije spasilac"
                });
            }
            else if(user.stationID != req.body.stationID) {
                console.log("error3")
                res.render('stanice', {
                    title: "Postavljanje voditelja stanica",
                    user: req.session.user,
                    stations: stations,
                    err: "Osoba nije član odabrane stanice"
                });
            }
            else if(user.role == "voditelj") {
                console.log("error4")
                res.render('stanice', {
                    title: "Postavljanje voditelja stanica",
                    user: req.session.user,
                    stations: stations,
                    err : "Osoba je već voditelj jedne stanice"
                });
            } else {
                let station = await Station.fetchByID(req.body.stationID)
                if(station.nameLeader !== null) {
                    let removeLeader = await User.removeLeader(station.nameLeader)
                }
                let nameLeader = await Station.setStationLeader(req.body.stationID, req.body.leader)
                let leaderSet = await User.setLeader(req.body.leader)
                res.redirect('/admin/stanice')
            }
        }
        else {
        res.redirect('/admin/stanice')
    }
} else {
    res.redirect("/")
}

    })();
});

router.post('/zahtjevi', function (req, res, next) {
    (async () => {
        if(req.session.user.role === "admin") {
        if (req.body.dodajButton !== undefined) {
            let approved = await User.approveUser(req.body.username)
        } else {
            let deleted = await User.deleteUser(req.body.username)
            //TODO
        }
        let notApproved = await User.getNotApprovedUsers();
        res.render('zahtjevi', {
            title: "Zahtjevi za registraciju",
            user: req.session.user,
            notApprovedUsers: notApproved
        });
    } else {
        res.redirect("/")
    }

    })();
})


module.exports = router;