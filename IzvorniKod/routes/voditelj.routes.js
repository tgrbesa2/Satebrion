var express = require('express')
var router = express.Router();
const pg = require('pg');
const User = require('../models/UserModel');
const Station = require('../models/StationModel');
const db = require('../db');

// Dohvat voditeljovog homepagea
router.get('/', function(req,res,next){
    (async () => {
        if (req.session.user.role !== "voditelj"){
            res.redirect("/")
        } else {
            console.log("voditelj route!")
            res.render('voditelj', {
                title:"HGSS Voditelj",
                user:req.session.user
            });
        }
    })();
})

router.get('/userprofile', function (req, res, next) {
    (async () => {
        res.redirect('/userprofile');

    })();
})

/* Osposobljenja router 
 potrebno je utvrditi ime postaje (getStationName(stationID))
 potrebno je utvrditi sve članove te postaje (getStationMembers(stationID)) */
router.get('/osposobljenja',function(req,res,next) {
    (async () => {
        if (req.session.user.role === "voditelj"){
        let station = await User.getStationName(req.session.user.stationID)
        let members = await User.getStationMembers(req.session.user.stationID)
        //console.log(members) Kontrola
        //console.log(station)
        res.render('osposobljenja',{
            user:req.session.user,
            namestation:station[0].namestation,
            members:members
        })
    }
    })();
})

router.get('/osposobljenja/usr/:username' , function(req,res,next) {
    (async () => {
        if (req.session.user.role === "voditelj"){
        let spasilac = await User.fetchByUsername(req.params.username)
        let osposobljenja = await User.getUserSkills(spasilac.username)

        let stringOsposobljenja = "";
        if(osposobljenja !== undefined){
            for(let i = 0; i < osposobljenja.length; i++) {
                if(i > 0) {
                    stringOsposobljenja += ", ";
                }
                stringOsposobljenja += osposobljenja[i].nameskill;
            }
        } 
        res.render('urediOsposobljenje' , {
            user:req.session.user,
            spasilac:spasilac,
            osposobljenja:stringOsposobljenja
        })
    }
    })()
})

router.post('/osposobljenja/usr/:username' , function(req,res,next) {
    (async () => {
        if (req.session.user.role === "voditelj"){
        let spasilac = await User.fetchByUsername(req.params.username)
        let zaDodatiSkills = req.body
        let vecSadrzaniSkills = await User.getUserSkills(spasilac.username)
        
        for(let nameSkill in zaDodatiSkills) {
            let flag = 1;
            if(vecSadrzaniSkills !== undefined) {
                for(let i = 0; i < vecSadrzaniSkills.length; i++) {
                    if(nameSkill === vecSadrzaniSkills[i].nameskill) {
                        flag = 0;
                        break;
                    }
                }
            }   
            if(flag == 1) {
               User.insertSkill(spasilac.username, nameSkill)
            }
        }        
        res.redirect('/voditelj/osposobljenja')
    }
    })();
})


router.get('/logout', function (req, res, next) {
    (async () => {
        res.redirect('/logout');

    })();
})

//dodavanje funkcionalnosti spasioca
router.get('/zahtjevi', function (req, res, next) {
    (async () => {
        if (req.session.user.role === "voditelj"){
        let misije = await User.getAvailableMissions(req.session.user.username)
        let dostupnost = await User.getAvailability(req.session.user.username)
        res.render('zahtjeviSpasilac' , {
            user:req.session.user,
            title:'Dostupni zahtjevi za spašavanje',
            missions: misije,
            availability: dostupnost
        })
    }
    })();
})

router.post('/zahtjevi/:idmission', function (req, res, next) {
    (async () => {
        let spasioc = req.session.user.username
        let x = await User.takeMission(spasioc, req.params.idmission)
        let y = await User.changeAvail(spasioc)
        let user = await User.fetchByUsername(spasioc)
        req.session.user = user
        res.redirect('/')
    })();
})

router.post('/dostupnost', function(req,res,next) {
    (async () => {
        let ime = req.body.ime
        let provjera = await Action.checkUserAction(ime)
        if(provjera) {
            res.redirect('/')
        } else {
            let promjena = await User.changeAvail(ime)
            let user = await User.fetchByUsername(ime)
            req.session.user = user
            res.redirect('/')
        }
    })();
})


module.exports = router;