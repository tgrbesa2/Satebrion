var express = require('express');
var router = express.Router();
const User = require ('../models/UserModel');


// Prikaz login stranice
router.get('/', function(req,res,next) {
    if(req.session.user !== undefined) {
        (async () => {
        let stationName = await User.getStationName(req.session.user.stationID)
        let userDB = await User.fetchByUsername(req.session.user.username)

        res.render('userprofile', {
            title : 'User profile',
            user: userDB,
            station: stationName[0].namestation,
            error:undefined
        });
    })()
    } else {
        res.redirect('/');
    }
});

module.exports = router;