const express = require('express');
const app = express();
const path = require('path');
const db = require('./db');
const session = require('express-session');
const fileUpload = require('express-fileupload');
const pgSession = require('connect-pg-simple')(session);


const PORT = process.env.PORT || 5000

// router setup
const homeRouter = require('./routes/home.routes');
const loginRouter = require('./routes/login.routes');
const logoutRouter = require('./routes/logout.routes'); // dodan router za logout
const registerRouter = require('./routes/register.routes');
const adminRouter = require('./routes/admin.routes'); // Dodan router za admin dashboard
const spasilacRouter = require('./routes/spasilac.routes') // Dodan router za spasilac dashboard
const voditeljRouter = require('./routes/voditelj.routes')
const userRouter = require('./routes/userprofile.routes')
const mapRouter = require('./routes/map.routes')
const dispecerRouter = require('./routes/dispecer.routes')

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(fileUpload());


app.use(express.static(path.join(__dirname, 'public')));
app.use(express.urlencoded({ extended: true }));

//session
app.use(session({
    store: new pgSession({
        pool: db.pool
    }),
    secret: 'Pee Pee',
    resave: false,
    saveUninitialized: true,
    cookie: {maxAge: 24 * 60 * 60 * 1000}, // 1 dan
}));

// views setup

// router setup
app.use('/', homeRouter);
app.use('/login', loginRouter);
app.use('/logout', logoutRouter);
app.use('/register',registerRouter);
app.use('/admin', adminRouter);
app.use('/spasilac', spasilacRouter);
app.use('/dispecer', dispecerRouter);
app.use('/voditelj', voditeljRouter);
app.use('/userprofile',userRouter);
app.use('/mapaDispecer', mapRouter);
app.use('/mapaUser', mapRouter);

var server = app.listen(PORT, () => console.log(`Listening on ${ PORT }`))

module.exports = server;