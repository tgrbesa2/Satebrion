var request = require('supertest')
var chai = require('chai')
var chaiHttp = require('chai-http')
var expect = chai.expect
const db = require('../db/index')

chai.use(chaiHttp)
chai.should()

describe('Testing spasilac functionality', function() {
    this.timeout(10000)
    var server;
    var authenticatedUser;

    before(function () {
        server = require('../server')
        authenticatedUser = request.agent(server)

    })
    beforeEach(function () {
        //server = require('../server')
    })

    afterEach(function (done) {
        //delete require.cache[require.resolve('../server')];
        //server.close()
        done()
    })

    
    after(function (done) {
        //db.query("DELETE FROM action WHERE actiontitle = 'spasavanje macke test'")
        //done()
        delete require.cache[require.resolve('../server')];
        server.close()
        done()
    })


    it('Test login with spasilac', function(done) {

        authenticatedUser
            .post("/login")
            .type('form')
            .send({'username':'spasilac',
                    'password':'spasilac'})
            .end((err, res) => {
                expect(res.status).to.equal(302)
                expect('Location', '/')
                done()
            })


    })

    it('Test userprofile', function(done) {

        authenticatedUser
            .get("/userprofile")
            .end((err, res)=> {
                expect(res.status).to.equal(200)
                expect('Location', '/userprofile')
                done()

            })
    })

    it('Test zahtjevi', function(done) {

        authenticatedUser
            .get("/spasilac/zahtjevi")
            .end((err, res)=> {
                expect(res.status).to.equal(200)
                expect('Location', '/spasilac/zahtjevi')
                done()

            })
    })
    
    it('Test logout with spasilac', function(done) {

        authenticatedUser
            .get("/logout")
            .end((err, res)=> {
                expect(res.status).to.equal(302)
                expect('Location', '/')
                done()

            })
    })



    
    
 

})