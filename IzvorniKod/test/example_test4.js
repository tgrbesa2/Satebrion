var request = require('supertest')
var chai = require('chai')
var chaiHttp = require('chai-http')
var expect = chai.expect
const db = require('../db/index')

chai.use(chaiHttp)
chai.should()

describe('Testing dispecer functionality', function() {
    this.timeout(15000)
    var server;
    var authenticatedUser;

    before(function () {
        server = require('../server')
        authenticatedUser = request.agent(server)
    })
    beforeEach(function () {

        //server = require('../server')
    })

    afterEach(function (done) {
        //delete require.cache[require.resolve('../server')];
        //server.close()

        done()
    })

    
    after(function (done) {
        db.query("DELETE FROM action WHERE actiontitle = 'spasavanje macke test'")
        //done()
        delete require.cache[require.resolve('../server')];
        server.close()
        done()
    })


    it('Test login with dispecer', function(done) {

        authenticatedUser
            .post("/login")
            .type('form')
            .send({'username':'dispecer',
                    'password':'dispecer'})
            .end((err, res) => {
                expect(res.status).to.equal(302)
                expect('Location', '/')
                done()
            })


    })

    it('Test userprofile', function(done) {

        authenticatedUser
            .get("/userprofile")
            .end((err, res)=> {
                expect(res.status).to.equal(200)
                expect('Location', '/userprofile')
                done()

            })
    })

    it('Test available spasioci', function(done) {

        authenticatedUser
            .get("/dispecer/spasioci")
            .end((err, res)=> {
                expect(res.status).to.equal(200)
                expect('Location', 'dispecer/spasioci')
                done()

            })
    })

    it('Test actions', function(done) {

        authenticatedUser
            .get("/dispecer/akcije")
            .end((err, res)=> {
                expect(res.status).to.equal(200)
                expect('Location', 'dispecer/akcije')
                done()

            })
    })

    it('Test get actions', function(done) {

        authenticatedUser
            .get("/dispecer/otvoriakciju")
            .end((err, res)=> {
                expect(res.status).to.equal(200)
                expect('Location', 'dispecer/otvoriakciju')
                done()

            })
    })


    it('Test Opening Action', function(done) {

        authenticatedUser
        .post("/dispecer/otvoriakciju")
        .type("form")
        .send({naslovakcije: "spasavanje macke test",
                opis: "test test test"})
        .end((err, res) => {
            expect(res.status).to.equal(302)
            expect('Location', '/dispecer/akcije')
            done()
        })
    })

    it('Test logout with dispecer', function(done) {

        authenticatedUser
            .get("/logout")
            .end((err, res)=> {
                expect(res.status).to.equal(302)
                expect('Location', '/')
                done()

            })
    })


    
    
 

})