var request = require('supertest')
var chai = require('chai')
var chaiHttp = require('chai-http')
var expect = chai.expect
const db = require('../db/index')

chai.use(chaiHttp)
chai.should()

describe('Testing login/logout functionality', function() {
    this.timeout(10000)
    var server;
    var authenticatedUser;


    before(function () {
        server = require('../server')
        authenticatedUser = request.agent(server)

    })
    beforeEach(function () {
        //server = require('../server')
    })

    afterEach(function (done) {
        //delete require.cache[require.resolve('../server')];
        //server.close()
        done()
    })

    
    after(function (done) {
        //db.query("DELETE FROM action WHERE actiontitle = 'spasavanje macke test'")
        //done()
        delete require.cache[require.resolve('../server')];
        server.close()
        done()
    })


    it('Test WRONG login with spasilac', function(done) {

        authenticatedUser
            .post("/login")
            .type("form")
            .send({'username':'spasilac',
                    'password':'asd'})
            .end((err, res) => {
                expect(res.status).to.equal(200)
                expect('Location', '/login')
                done()
            })

    })

    it('Test WRONG login, trying to get userprofile', function(done) {


        authenticatedUser
            .get("/userprofile")
            .end((err, res)=> {
                expect(res.status).to.equal(302)
                expect('Location', '/')
                done()

            })
    })

    it('Test login with spasilac', function(done) {

        authenticatedUser
            .post("/login")
            .type('form')
            .send({'username':'spasilac',
                    'password':'spasilac'})
            .end((err, res) => {
                expect(res.status).to.equal(302)
                expect('Location', '/')
                done()
            })


    })

    it('Test logout with spasilac', function(done) {

        authenticatedUser
            .get("/logout")
            .end((err, res)=> {
                expect(res.status).to.equal(302)
                expect('Location', '/')
                done()

            })
    })



    
    
 

})