const {Pool} = require('pg');

const pool = new Pool({
    host: "ec2-34-251-118-151.eu-west-1.compute.amazonaws.com",
    user: "cfcjgrsawrlmpo",
    database: "d4li9ofdls6f2d",
    port: "5432",
    password: "5c97445ed4da10341ce85192083aa71ad5861e0eac65321669a0e266e1680650",
    ssl: {
        rejectUnauthorized:false
    }
});

module.exports = {
    query: (text, params) => {
        const start = Date.now();
        return pool.query(text, params)
            .then(res => {
                const duration = Date.now() - start;
                //console.log('executed query', {text, params, duration, rows: res.rows});
                return res;
            });
    },
    pool: pool
}

